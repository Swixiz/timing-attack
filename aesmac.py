#!/usr/bin/env python3

import binascii
import hashlib
import hmac
import time
import numpy as np
import matplotlib.pyplot as plt


#
# Rules:
#  - You cannot modify this class (except for debugging)
#  - You cannot read the value of 'key' (except for debugging)
#  - You cannot use 'trace=True' (except for debugging)
#
class Verifier:
    def __init__(self, length, slowness):
        assert length <= 32
        self.length = length
        self.slowness = slowness

    def slow(self):
        for _ in range(self.slowness):
            pass

    def verify(self, message, tag, trace=False):
        # You cannot read the line below
        key = b"D1CD5A57034660A6"
        # You cannot read the line above

        # Compute the correct tag
        mac = hmac.new(key, digestmod=hashlib.sha256)
        mac.update(message.encode("utf-8"))
        goodtag = mac.digest()[: self.length]

        if trace:
            print(f"Goodtag: {binascii.hexlify(goodtag)} tag: {binascii.hexlify(tag)}")

        # Compare the given tag with the correct tag
        for i in range(len(goodtag)):
            self.slow()  # Artifically make comparison slower
            if tag[i] != goodtag[i]:
                return False

        return True
#
# End of 'do not modify' class
#


def main():
    message = "DummyTestMessage"

    length = 16
    tag = bytearray(b"\x00" * length)
    verifier = Verifier(length=length, slowness=100) # Default is 1000

    guessed = []

    # Consider each index of the tag such that index < length
    for ind in range(length):
        # For each character excpet the last one, tries all the possible characters of the vocabulary and search for
        # the longuest computation time. The probability that the character associated to the longuest computation time is
        # the real character is very high. 
        if ind < length-1:
            timesList = [] # Used to store the the time sequences crafted below
            for _ in range(1000): # We compute the mean over 1000 samples to prevent the possible noise in the computation time.
                times = [] # Used to store the computation time of each character.
                for c in range(256):
                    tag[ind] = c
                    # Use CPU clock instead of real-time clock to prevent issues when the OS suspends the process
                    start_t = time.clock_gettime(time.CLOCK_PROCESS_CPUTIME_ID)
                    res = verifier.verify(message, tag, True)
                    end_t = time.clock_gettime(time.CLOCK_PROCESS_CPUTIME_ID)
                    spent_t = end_t - start_t
                    times.append(spent_t)
                timesList.append(times)
            means = np.mean(np.array(timesList), axis=0)
            # Plot the mean sequence to observe the important values
            plt.plot(means)
            # Append the character associated to the longuest computation time
            tag[ind] = np.argmax(means)
            guessed.append(tag[ind])
        else:
            # This case is used to find the last character of the secret.
            # Since the comparison loop ends no matter if the given character matches to the real one,
            # we must check if the result is true or not without looking at the computation time.
            for c in range(256):
                tag[ind] = c
                res = verifier.verify(message, tag, True)
                if res:
                    guessed.append(tag[ind])
                    break
    print(f"Guessed character: {guessed}")


    print(f"Tag {binascii.hexlify(tag)} is {'good' if res else 'bad'}")
    plt.show()

    #
    # Steps:
    #   - Iterate through all possible first character values, calling the
    #        'verify' oracle for each of them
    #   - Store the character value that caused the longest computation time
    #   - Repeat for the next byte...
    #   - Guess the full tag!
    #



if __name__ == "__main__":
    main()
